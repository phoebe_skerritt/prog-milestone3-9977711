﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milestone3_9977711
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Variables used
            string pizzaflavour;
            int i;
            int x;
            var order = new List<Tuple<string, string, double>>();
            double cost = 0;
            string owing;
            double payment = 0;

            //welcome customer
            Customer.cdetails();

            //lists holding the type of pizza
            var flavour = new List<string> { "Meatlovers", "Hawaiian", "Ham and Cheese", "Vegetarian", "Beef and Onion", "Cheese" };

            //lists holding the size of pizza to calculate the right price
            List<Tuple<string, double>> pizza = new List<Tuple<string, double>>();
            pizza.Add(Tuple.Create("Small", 5.00));
            pizza.Add(Tuple.Create("Medium", 7.50));
            pizza.Add(Tuple.Create("Large", 11.00));

            //lists holding the choice of drinks
            List<Tuple<string, double>> drink = new List<Tuple<string, double>>();
            drink.Add(Tuple.Create("Orange Juice", 4.50));
            drink.Add(Tuple.Create("Coke", 4.50));
            drink.Add(Tuple.Create("Lemonade", 4.50));
            drink.Add(Tuple.Create("Raspberry", 4.50));

            //lists holding desert options
            List<Tuple<string, double>> desert = new List<Tuple<string, double>>();
            desert.Add(Tuple.Create("Chocolate Mousse", 4.00));
            desert.Add(Tuple.Create("Vanilla Mousse", 4.00));
            desert.Add(Tuple.Create("Strawberry Cheesecake", 5.00));
            desert.Add(Tuple.Create("Banana Split", 6.00));


        //Menu screen to place an order
        menu:

            Console.Clear();
            Console.WriteLine("Welcome to Phoebe's Pizza Palace");
            Console.WriteLine();
            Console.WriteLine("Menu Select:");
            Console.WriteLine("1. Pizza");
            Console.WriteLine("2. Drinks");
            Console.WriteLine("3. Dessert");
            Console.WriteLine("4. Place Order");
            Console.WriteLine();
            Orderdetails.display();


            //counting the pizzas for the end order
            if (int.TryParse(Console.ReadLine(), out x))
            {
                switch (x)
                {
                    case 1:
                    Pizzastart:
                        Console.Clear();
                        Console.WriteLine();
                        for (i = 0; i < flavour.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {flavour[i]}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}. Return to menu");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("Please try again");
                                Console.ReadLine();
                                goto Pizzastart;
                            }
                            else
                            {
                                pizzaflavour = flavour[x - 1];
                                Console.Clear();
                                Orderdetails.display();
                                for (i = 0; i < pizza.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {pizza[i].Item1}");
                                }
                                Console.WriteLine();
                                Console.WriteLine($"{i + 1}. Return to menu");

                                if (int.TryParse(Console.ReadLine(), out x))
                                {
                                    if (x == i + 1)
                                    {
                                        goto menu;
                                    }
                                    else if (x > i + 1)
                                    {
                                        Console.WriteLine("Please try again");
                                        Console.ReadLine();
                                        goto Pizzastart;
                                    }
                                    else
                                    {
                                        order.Add(Tuple.Create(pizzaflavour, pizza[x - 1].Item1, pizza[x - 1].Item2));
                                        Orderdetails.order.Add(Tuple.Create(pizzaflavour, pizza[x - 1].Item1, pizza[x - 1].Item2));
                                    }
                                    goto Pizzastart;
                                }
                                else
                                {

                                    Console.WriteLine("Please try again");
                                    Console.ReadLine();
                                    goto Pizzastart;
                                }
                            }

                        }
                        else
                        {

                            Console.WriteLine("Please try again.");
                            Console.ReadLine();
                            goto Pizzastart;
                        }

                    //count for drinks
                    case 2:
                    Drinkstart:

                        Console.Clear();
                        for (i = 0; i < drink.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {drink[i].Item1}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}. Return to menu");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("Please try again");
                                Console.ReadLine();
                                goto Drinkstart;
                            }
                            else
                            {
                                order.Add(Tuple.Create("Drink     ", drink[x - 1].Item1, drink[x - 1].Item2));
                                Orderdetails.order.Add(Tuple.Create("Drink     ", drink[x - 1].Item1, drink[x - 1].Item2));
                            }
                            goto Drinkstart;
                        }
                        else
                        {
                            Console.WriteLine("Please try again");
                            goto Drinkstart;
                        }

                    //count for deserts
                    case 3:
                    desertstart:

                        Console.Clear();
                        for (i = 0; i < desert.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {desert[i].Item1}");
                        }

                        Console.WriteLine($"{i + 1}. Return to menu");


                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("Please try again");
                                Console.ReadLine();
                                goto desertstart;
                            }
                            else
                            {
                                order.Add(Tuple.Create("Desert     ", desert[x - 1].Item1, desert[x - 1].Item2));
                                Orderdetails.order.Add(Tuple.Create("Desert     ", desert[x - 1].Item1, desert[x - 1].Item2));
                            }

                            goto desertstart;
                        }
                        else
                        {
                            Console.WriteLine("Please try again");
                            Console.Clear();
                            goto desertstart;
                        }

                    //display order and check with customer if its right - also counting everything thats been ordered
                    case 4:
                        Console.Clear();
                        Orderdetails.display();
                        Console.WriteLine("Are you happy with your order?");
                        Console.WriteLine("1. Yes");
                        Console.WriteLine("2. No");
                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            switch (x)
                            {
                                case 1:
                                    Console.Clear();
                                    for (i = 0; i < order.Count; i++)
                                    {
                                        owing = String.Format("{0:C}", order[i].Item3);
                                        Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {owing}");
                                        cost = cost + order[i].Item3;
                                    }
                                    owing = String.Format("{0:C}", cost);
                                    Console.WriteLine();
                                    Console.WriteLine($"cost:   ${cost}");
                                    Console.WriteLine();

                                    Console.WriteLine("Please enter the amount of money you are going to pay with");
                                    payment = double.Parse(Console.ReadLine());

                                    if (payment > cost)
                                    {
                                        payment = payment - cost;
                                        Console.WriteLine($"Your change is ${payment}");
                                        Console.WriteLine("Thankyou, have a great day!");
                                        Console.ReadLine();
                                    }
                                    else if (payment < cost)
                                    {
                                        cost = cost - payment;
                                        Console.WriteLine("It looks like you don't have enough money.");
                                        Console.ReadLine();
                                    }
                                    Environment.Exit(0);
                                    break;

                                case 2:
                                    goto menu;
                            }
                        }
                        break;

                    default:
                        Console.WriteLine("Pleae try again");
                        Console.ReadLine();
                        goto menu;
                }
            }
            else
            {
                Console.WriteLine("Pleae try again");
                Console.ReadLine();
                goto menu;
            }
            Orderdetails.display();
        }
    }

    //greeting the customer
    public static class Customer
    {
        public static string name;
        public static string address;


        public static void cdetails()
        {
            Console.WriteLine("Hello and welcome to Phoebe's Pizza Plaza");
            Console.WriteLine("Please enter your name?");
            name = Console.ReadLine();
            Console.WriteLine("Please enter your address");
            address = Console.ReadLine();
            Console.Clear();

        }

    }
    //order details  with the customers details
    public static class Orderdetails
    {

        public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

        public static void display()
        {
            int i;
            double cost = 0;
            string owing;


            Console.WriteLine($"Name: {Customer.name}");
            Console.WriteLine($"Address: {Customer.address}");

            Console.WriteLine("Order Summary:");

            for (i = 0; i < order.Count; i++)
            {
                owing = String.Format("{0:C}", order[i].Item3);
                Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {owing}");
                cost = cost + order[i].Item3;
            }
            owing = String.Format("{0:C}", cost);
            Console.WriteLine();
            Console.WriteLine($"                cost:              ${cost}");

        }

    }



}